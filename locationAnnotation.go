package etcddata

type FromStation struct {
	LocalCode string `json:"LocalCode,omitempty"`
}
type OnRoute struct {
	FromStation FromStation `json:"FromStation,omitempty"`
}
type Location struct {
	OnRoute OnRoute `json:"OnRoute,omitempty"`
	Train   string  `json:"train,omitempty"`
}
